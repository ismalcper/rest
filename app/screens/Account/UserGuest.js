import React from "react";
import { StyleSheet, ScrollView, View, Text, Image } from "react-native";
import { Button } from "react-native-elements";
import { withNavigation } from "react-navigation";
import { LinearGradient } from "expo-linear-gradient";

function UserGuest(props) {
  const { navigation } = props;

  return (
    <View style={{ height: "100%" }}>
      {/* <LinearGradient
        colors={["rgb(173, 255, 47)", "transparent"]}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          height: "100%",
        }} 
      />*/}
      <ScrollView style={styles.viewbody} centerContent={true}>
        <Image
          source={require("../../../assets/img/user-guest.jpg")}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.title}>Consulta tu perfil de Rest</Text>
        <Text style={styles.description}>
          ¿Como describirias tu mejor restaurante? Busca y visualiza los mejores
          restaurantes de una forma sensilla, vota cual te ha gustado mas y
          comenta como ha sido tu experiencia.
        </Text>
        <View style={styles.viewbtn}>
          <Button
            buttonStyle={styles.btnStyles}
            containerStyle={styles.btnContainer}
            title="Ver tu perfil"
            onPress={() => navigation.navigate("Login")}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export default withNavigation(UserGuest);

const styles = StyleSheet.create({
  viewbody: {
    marginLeft: 30,
    marginRight: 30,
  },
  image: {
    height: 300,
    width: "100%",
    marginBottom: 40,
  },
  title: {
    fontWeight: "bold",
    fontSize: 19,
    marginBottom: 10,
    textAlign: "center",
  },
  description: {
    textAlign: "center",
    marginBottom: 20,
  },
  viewbtn: {
    flex: 1,
    alignItems: "center",
  },
  btnStyles: {
    backgroundColor: "#00a680",
  },
  btnContainer: {
    width: "70%",
  },
});
