import React, { useRef } from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import { Divider } from "react-native-elements";
import { withNavigation } from "react-navigation";
import LoginForm from "../../components/Account/LoginForm";
import Toast from "react-native-easy-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LoginFacebook from "../../components/Account/LoginFacebook";
import { LinearGradient } from "expo-linear-gradient";

function Login(props) {
  const { navigation } = props;
  const toastRef = useRef();
  return (
    <View style={{ height: "100%" }}>
      {/*  <LinearGradient
        colors={["rgb(173, 255, 47)", "transparent"]}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          height: "100%",
        }} 
      />*/}
      <ScrollView>
        <KeyboardAwareScrollView>
          <Image
            source={require("../../../assets/img/5-tenedores-letras-icono-logo.png")}
            style={styles.logo}
            resizeMode="contain"
          />
          <View style={styles.viewContainer}>
            <LoginForm toastRef={toastRef} />
            <CreateAccount navigation={navigation} />
          </View>
          <Divider style={styles.divider} />
          <View style={styles.viewContainer}>
            <LoginFacebook toastRef={toastRef} navigation={navigation} />
          </View>
          <Toast ref={toastRef} position="center" opacity={0.7} />
        </KeyboardAwareScrollView>
      </ScrollView>
    </View>
  );
}

export default withNavigation(Login);

function CreateAccount(props) {
  const { navigation } = props;
  return (
    <Text style={styles.textRegister}>
      ¿Aun no tienes cuenta?
      <Text
        style={styles.btnRegister}
        onPress={() => navigation.navigate("Register")}
      >
        Registrarte
      </Text>
    </Text>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 150,
    marginTop: 30,
  },
  viewContainer: {
    marginRight: 40,
    marginLeft: 40,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 40,
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
  },
  btnRegister: {
    color: "#00a680",
    fontWeight: "bold",
  },
});
