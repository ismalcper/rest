import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Avatar, Image } from "react-native-elements";
import * as firebase from "firebase";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Modal from "../Modal";

export default function InfoUser(props) {
  const [isVisible, setIsVisible] = useState(false);
  const {
    userInfo: { uid, photoURL, displayName, email },
    setReloadData,
    toastRef,
    setIsVisibleLoading,
    setTextLoading
  } = props;
  const changeAvatar = async () => {
    const resultPermissions = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    const resultPermissionsCamera =
      resultPermissions.permissions.cameraRoll.status;
    if (resultPermissionsCamera === "denied") {
      toastRef.current.show("Es necesario aceptar los permisos de la galeria");
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });
      if (result.cancelled) {
        toastRef.current.show("Has cerrado la galeria de imagenes");
      } else {
        uploadImage(result.uri, uid).then(() => {
          updatePhotoUrl(uid);
        });
      }
    }
  };

  const uploadImage = async (uri, nameImage) => {
    setTextLoading("Actualizando Avatar");
    setIsVisibleLoading(true);
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase
      .storage()
      .ref()
      .child(`avatar/${nameImage}`);

    return ref.put(blob);
  };

  const updatePhotoUrl = uid => {
    firebase
      .storage()
      .ref(`avatar/${uid}`)
      .getDownloadURL()
      .then(async result => {
        const update = {
          photoURL: result
        };
        await firebase.auth().currentUser.updateProfile(update);
        setReloadData(true);
        setIsVisibleLoading(false);
      })
      .catch(() => {
        toastRef.current.show("Error al recuperar la imagen del servidor");
      });
  };

  return (
    <View style={styles.viewUserInfo}>
      <Avatar
        rounded
        size="large"
        showEditButton
        onEditPress={changeAvatar}
        containerStyle={styles.userInfoAvatar}
        onPress={() => setIsVisible(true)}
        source={{
          uri: photoURL
            ? photoURL
            : "https://api.adorable.io/avatars/216/abott@adorable.png"
        }}
      />
      <View>
        <Text style={styles.displayName}>
          {displayName ? displayName : "Anónimo"}
        </Text>
        <Text>{email ? email : "Social Rest"}</Text>
      </View>

      <Modal isVisible={isVisible} setIsVisible={setIsVisible}>
        <Image
          source={{
            uri: photoURL
              ? photoURL
              : "https://api.adorable.io/avatars/216/abott@adorable.png"
          }}
          style={{ width: 350, height: 260 }}
        />
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  viewUserInfo: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#f2f2f2",
    paddingTop: 30,
    paddingBottom: 30
  },
  userInfoAvatar: {
    marginRight: 20
  },
  displayName: {
    fontWeight: "bold"
  }
});
