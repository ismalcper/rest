import { createStackNavigator } from "react-navigation-stack";
import SearchsScreen from "../screens/Search";

const SearchsScreenStacks = createStackNavigator({
  Search: {
    screen: SearchsScreen,
    navigationOptions: () => ({
      title: "Buscador"
    })
  }
});

export default SearchsScreenStacks;
